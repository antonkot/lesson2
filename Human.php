<?php

/**
 * Класс "Человек"
 */

class Human
{
	// Имя
	private $_name;

	// Одежда
	public $clothes;

	// Игрушки
	public $toys = [];

	/**
	 * Рождение человека
	 */
	function __construct($name)
	{
		// Присвоить имя
		$this->_name = $name;
	}

	// Кушать
	public function eat()
	{
		return 'Ом-ном-ном! ';
	}

	// Одеваться
	public function wear()
	{
		return $this->clothes;
	}

	// Играть
	public function play()
	{
		return sprintf(
			'%s: Я играю в %s. <br>',
			$this->_name,
			implode(', ', $this->toys)
		);
	}

	// Кричать
	public function scream()
	{
		return 'Ээээээ!';
	}

	public function __toString()
	{
		return sprintf(
			'Привет, меня зовут %s.<br>',
			$this->_name
		);
	}
}

?>
