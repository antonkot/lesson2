<?php

/**
* Класс "Мальчик"
*/
class Boy extends Human
{
	/**
	 * Рождение мальчика
	 */
	function __construct($name, $toys = ['машинки','солдатики','конструктор'])
	{
		// Вызываем конструктор класса "Человек"
		parent::__construct($name);

		// Мальчики носят штаны
		$this->clothes = 'штаны';

		// И играют в игрушки для мальчиков
		$this->toys = $toys;
	}

	// Мальчики умеют драться
	public function fight()
	{
		return 'Дыщ!';
	}

	// Мальчики кричат низко
	public function scream()
	{
		return 'Оооооо! ';
	}
}

?>
