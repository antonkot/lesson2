<?php

/**
* Класс "Девочка"
*/
class Girl extends Human
{
	/**
	 * Рождение девочки
	 */
	function __construct($name, $toys = ['куклы','посуда','фенечки'])
	{
		// Вызываем конструктор класса "Человек"
		parent::__construct($name);

		// Девочки носят юбки
		$this->clothes = 'юбка';

		// И играют в игрушки для девочек
		$this->toys = $toys;
	}

	// Девочки иногда плачут
	public function cry()
	{
		return 'Хнык!';
	}

	// Девочки кричат высоко
	public function scream()
	{
		return 'Аааааа! ';
	}
}

?>
