<?php

/**
 * Smile Expo PHP Learning Lesson 2
 */

// Подключаем нужные классы
require_once 'Human.php';
require_once 'Boy.php';
require_once 'Girl.php';

// Создаем группу детей
$children = [

	// Мальчики
	new Boy('Саша'),
	new Boy('Петя', ['приставка']),
	new Boy('Вася'),
	new Boy('Миша', ['самолетик']),
	new Boy('Лёша'),

	// Девочки
	new Girl('Маша'),
	new Girl('Даша', ['раскраска']),
	new Girl('Катя'),
	new Girl('Лена', ['мячик']),
	new Girl('Лера')
];

/**
 * Текстовое представление объектов
 */
echo '<h2>Здравствуйте, дети!</h2>';

foreach ($children as $child) {
	echo $child;
}
echo '<br>';

/**
 * Метод, зависящий от свойств класса
 */
echo '<h2>Давайте поиграем</h2>';

foreach ($children as $child) {
	echo $child->play();
}
echo '<br>';

/**
 * Метод, определенный в классе-родителе
 */
echo '<h2>Обед!</h2>';

foreach ($children as $child) {
	echo $child->eat();
}
echo '<br><br>';

/**
 * Переопределенный метод
 */
echo '<h2>Паууук!!!</h2>';

foreach ($children as $child) {
	echo $child->scream();
}
echo '<br>';

?>
